import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mainapp-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  openRuleModel() {
    const busEvent = new CustomEvent('app-event-bus', {
      bubbles: true,
      detail: {
        eventType: 'rule-module',
        customData: 'take me to rule module'
      }
    });
      dispatchEvent(busEvent);
  }

  redirectToHomePage() {
      const busEvent = new CustomEvent('app-event-bus', {
        bubbles: true,
        detail: {
          eventType: 'home-page',
          customData: 'take me to Home Page module'
        }
      });
        dispatchEvent(busEvent);    
  }

}
