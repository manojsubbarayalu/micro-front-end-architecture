import { Component } from '@angular/core';
import { fromEvent } from 'rxjs';

@Component({
  selector: 'app2-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app2';
  $eventBus: any;
  auth: any;
  router: any;
  showDashboardSection = true;

  ngOnInit() {
    this.$eventBus = fromEvent<CustomEvent>(window, 'app-event-bus').subscribe((e) =>   this.onEventHandler(e));
  }
  
  onEventHandler(e: CustomEvent) {
    if (e.detail.eventType === 'rule-module') {
      //alert(e.detail.customData);
      this.showDashboardSection = false;
    } else if(e.detail.eventType === 'home-page') {
      this.showDashboardSection = true;
    }
  }

  goToHomePage() {
    const busEvent = new CustomEvent('app-event-bus', {
      bubbles: true,
      detail: {
        eventType: 'home-page',
        customData: 'take me to Home Page module'
      }
    });
      dispatchEvent(busEvent);
  }
}


